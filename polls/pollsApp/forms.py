import re
from django import forms
from django.forms import ModelForm
from django.forms.models import inlineformset_factory
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


from .models import Poll, Question, Answer
 
class RegistrationForm(forms.Form):
 
    username = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Username"), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
    email = forms.EmailField(widget=forms.TextInput(attrs=dict(required=True, max_length=30)), label=_("Email address"))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password"))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs=dict(required=True, max_length=30, render_value=False)), label=_("Password (again)"))
 
    def clean_username(self):
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("The username already exists. Please try another one."))
 
    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields did not match."))
        return self.cleaned_data



class PollForm(ModelForm):

    # username = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=100)), label=_("name"))
    # author = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("author"))
    # weight = forms.IntegerField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("weight"))

    class Meta:
        model = Poll
        fields = ('name', 'weight')

class QuestionForm(ModelForm):

    # username = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=100)), label=_("name"))
    # author = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("author"))
    # weight = forms.IntegerField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("weight"))

    class Meta:
        model = Question
        fields = "__all__"

class AnswerForm(ModelForm):

    # username = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=100)), label=_("name"))
    # author = forms.CharField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("author"))
    # weight = forms.IntegerField(widget=forms.TextInput(attrs=dict(required=True, max_length=50)), label=_("weight"))

    class Meta:
        model = Answer
        fields = "__all__"

# QuestionFormSet = inlineformset_factory(Poll, Question, form=QuestionForm, fields = '__all__', extra=30, can_delete=True)
# AnswerFormSet = inlineformset_factory(Question, Answer, form=AnswerForm, fields = '__all__', extra=30, can_delete=True)
