from django.shortcuts import render

from .forms import *
from .models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import CreateView


@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1'],
            email=form.cleaned_data['email']
            )
            return HttpResponseRedirect('/register/success/')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {
    'form': form
    })

    return render_to_response(
    'registration/register.html',
    variables,
    )

def register_success(request):
    return render_to_response(
    'registration/success.html',
    )

def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')

def main(request):

    polls = Poll.objects.all()

    return render_to_response(
        'main.html',
        {
            'user': request.user,
            'polls': polls,
        }
    )

@login_required
def home(request):
    return render_to_response(
    'home.html',
    { 'user': request.user }
    )


class PollCreateView(CreateView):
    template_name = 'poll_add.html'
    model = Poll
    form_class = PollForm
    success_url = '/home/'

    def get_context_data(self, **kwargs):
        context = super(PollCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['question_form'] = QuestionFormSet(self.request.POST)
            context['answer_form'] = AnswerFormSet(self.request.POST)
        else:
            context['question_form'] = QuestionFormSet()
            context['answer_form'] = AnswerFormSet()
        return context

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        question_form = QuestionFormSet()
        answer_form = AnswerFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  question_form=question_form,
                                  answer_form=answer_form))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        question_form = QuestionFormSet(self.request.POST)
        answer_form = AnswerFormSet(self.request.POST)
        if (form.is_valid() and question_form.is_valid() and
            answer_form.is_valid()):
            return self.form_valid(form, question_form, answer_form)
        else:
            return self.form_invalid(form, question_form, answer_form)

    def form_valid(self, form, question_form, answer_form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        self.object = form.save()
        question_form.instance = self.object
        question_form.save()
        answer_form.instance = self.object
        answer_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, question_form, answer_form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form,
                                  question_form=question_form,
                                  answer_form=answer_form))

@login_required
class PollCreate(CreateView):
    template_name = 'poll_add.html'
    form_class = PollForm
    success_url = '/home/'
    model = Poll
    fields = ['name', 'weight']

    def form_valid(self, form):
        form.instance.author = self.request.user
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())
