from django.db import models
from django.contrib.auth.models import User


class Poll(models.Model):
    name = models.CharField(max_length=100)
    author = models.ForeignKey(User)
    votes = models.IntegerField(blank=True, null=True)
    weight = models.IntegerField(choices=[(i, i) for i in range(11)], default=10)

class Question(models.Model):
    question = models.TextField()
    votes = models.IntegerField(blank=True, null=True)
    poll = models.ForeignKey(Poll)

class Answer(models.Model):
    answer = models.TextField()
    votes = models.IntegerField(blank=True, null=True)
    question = models.ForeignKey(Question)